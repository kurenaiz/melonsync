local utils = {}

function utils.dump(o)
	if type(o) == 'table' then
		local s = '{ '
		for k, v in pairs(o) do
			if type(k) ~= 'number' then k = '"' .. k .. '"' end
			s = s .. '[' .. k .. '] = ' .. utils.dump(v) .. ','
		end
		return s .. '} '
	else
		return tostring(o)
	end
end

-- IP_TYPE
utils.R = { ERROR = 10, IPV4 = 11, IPV6 = 12, STRING = 13 }
-- validates the IP,  returns IP_TYPE
function utils.validate_ip(ip)
	if type(ip) ~= "string" then return utils.R.ERROR end

	-- check for format 1.11.111.111 for ipv4
	local chunks = { ip:match("^(%d+)%.(%d+)%.(%d+)%.(%d+)$") }
	if #chunks == 4 then
		for _, v in pairs(chunks) do
			if tonumber(v) > 255 then return utils.R.STRING end
		end
		return utils.R.IPV4
	end

	-- check for ipv6 format, should be 8 'chunks' of numbers/letters
	-- without leading/trailing chars
	-- or fewer than 8 chunks, but with only one `::` group
	chunks = { ip:match("^" .. (("([a-fA-F0-9]*):"):rep(8):gsub(":$", "$"))) }
	if #chunks == 8
	    or #chunks < 8 and ip:match('::') and not ip:gsub("::", "", 1):match('::') then
		for _, v in pairs(chunks) do
			if #v > 0 and tonumber(v, 16) > 65535 then return utils.R.STRING end
		end
		return utils.R.IPV6
	end

	return utils.R.STRING
end

function utils.split(s, delimiter)
	local result = {}
	for match in (s .. delimiter):gmatch("(.-)" .. delimiter) do
		table.insert(result, match)
	end
	return result
end

return utils
