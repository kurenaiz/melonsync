-- !!! stolen from !!!
-- https://github.com/po5/mpv_irc/blob/master/scrollingchat_buffer.lua
-- sorry! :3c

local config = require('config')
local chat = require('chat')

local chat_style = {}
local chat_buffer = {}
local chat_buffer_size = 0
-- max messages at the same time
local chat_buffer_max_size = 100

local rendering = false

-- TODO: set on config later
local speed = 2.222
local ass_style = "\\1c&HC8C8B4\\fs33\\bord2"

local function round(num)
	m = num % 50
	if m == 0 then m = 50 end
	return num + (50 - (m))
end

local function on_message_add(text, data)
	local w, h = mp.get_osd_size()

	if #chat_buffer > chat_buffer_max_size then
		table.remove(chat_buffer, 1)
	end

	local msg = {}
	msg.y = round(math.random(15, h - 51))
	if chat_buffer[msg.y] then
		if chat_buffer[msg.y].x <= w / 2 then
			chat_buffer[msg.y .. "-" .. os.time()] = chat_buffer[msg.y]
			-- mp.add_timeout(chat_buffer[msg.y].x / 2 / (speed * (speed / 0.001)), function() delete end)
		end
	end
	msg.x = w
	msg.content = text:gsub("^&br!", ""):gsub("&br!", "\\N")
	chat_buffer[msg.y] = msg
	if not rendering then
		danmaku_timer:resume()
	end
end

local function render_chat(ass)
	rendering = true
	ass:new_event()
	ass:an(7)
	ass:pos(2, 2)
	ass:append('\\N')
	for key, msg in pairs(chat_buffer) do
		ass:new_event()
		ass:append(string.format("{\\pos(%s,%s)%s}", msg.x, msg.y, ass_style))
		ass:append(msg.content:gsub("(>.-\\N)", "{\\1c&H35966f&}%1"):gsub("(\\N[^>])", "{\\1c&HC8C8B4}%1"))

		if msg.x < -2500 then chat_buffer[key] = nil end
	end

	if ass.text == "" then
		rendering = false
		danmaku_timer:kill()
	end
end

function loop_render_chat()
	chat.update()
	for key, msg in pairs(chat_buffer) do
		msg.x = msg.x - speed
	end
end

-- Empty the log buffer of all messages (Ctrl+L)
local function clear_chat_buffer()
	chat_buffer = {}
	chat.update()
end

local function on_set_bindings(bindings)
	table.insert(bindings, { 'ctrl+l', clear_chat_buffer })
end

function chat_style.setup()
	chat.on_message_add = on_message_add
	chat.on_set_bindings = on_set_bindings
	chat.render_bus = render_chat
	danmaku_timer = mp.add_periodic_timer(0.001, loop_render_chat)
end

return chat_style
