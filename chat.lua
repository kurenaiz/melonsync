-- Includes modified code from mpv/lua/console.lua
-- Copyright (C) 2019 the mpv developers
--
-- Permission to use, copy, modify, and/or distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
-- SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
-- OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
-- CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
local utils = require('mp.utils')
local assdraw = require('mp.assdraw')
local config = require('config')

local chat = {
	-- EVENT: on message is sent
	--   when you press enter with the repl active
	-- function on_input_submit(input)
	on_input_submit = nil,
	-- EVENT: on message added to the chat
	--   both own messages, and from roommates (socket messages)
	-- function on_message_add(msg, data)
	on_message_add = nil,
	-- EVENT: on open input
	-- function on_open_input()
	on_open_input = nil,
	-- EVENT: on set bindings
	-- function on_set_bindings(bindings)
	on_set_bindings = nil,
	render_bus = nil,
	-- if the input is open
	repl_active = false,
	insert_mode = false,
	history = {},
	line = '',
	-- placeholder if nothing typed
	line_placeholder = '',
	-- placeholder if nothing typed
	line_prefix = '> ',
	on_join_cb = nil
}

local cursor = 1
local history_pos = 1

local key_bindings = {}

function chat.update()
	local dpi_scale = mp.get_property_native("display-hidpi-scale", 1.0)

	dpi_scale = dpi_scale * config.scale

	local screenx, screeny, aspect = mp.get_osd_size()
	screenx = screenx / dpi_scale
	screeny = screeny / dpi_scale

	local ass = assdraw.ass_new()

	if chat.render_bus then
		chat.render_bus(ass)
	end

	-- Clear the OSD if the REPL is not active
	if not chat.repl_active then
		mp.set_osd_ass(screenx, screeny, ass.text)
		return
	end

	local style = '{\\r' ..
	    '\\1a&H00&\\3a&H00&\\4a&H99&' ..
	    '\\1c&Heeeeee&\\3c&H111111&\\4c&H000000&' ..
	    '\\fn' .. config.font .. '\\fs' .. config.font_size ..
	    '\\bord1\\xshad0\\yshad1\\fsp0\\q1}'
	local text_style = '{\\r' ..
	    '\\1a&H00&\\3a&H00&\\4a&H99&' ..
	    '\\1c&H999999&\\3c&H111111&\\4c&H000000&' ..
	    '\\fn' .. config.font .. '\\fs' .. config.font_size ..
	    '\\bord1\\xshad0\\yshad1\\fsp0\\q1}'
	-- Create the cursor glyph as an ASS drawing. ASS will draw the cursor
	-- inline with the surrounding text, but it sets the advance to the width
	-- of the drawing. So the cursor doesn't affect layout too much, make it as
	-- thin as possible and make it appear to be 1px wide by giving it 0.5px
	-- horizontal borders.
	local cheight = config.font_size * 8
	local cglyph = '{\\r' ..
	    '\\1a&H44&\\3a&H44&\\4a&H99&' ..
	    '\\1c&Heeeeee&\\3c&Heeeeee&\\4c&H000000&' ..
	    '\\xbord0.5\\ybord0\\xshad0\\yshad1\\p4\\pbo24}' ..
	    'm 0 0 l 1 0 l 1 ' .. cheight .. ' l 0 ' .. cheight ..
	    '{\\p0}'

	ass:new_event()
	ass:an(7)
	ass:pos(2, 2)
	if chat.line == '' then
		ass:append(style .. chat.line_prefix .. text_style .. chat.line_placeholder)
		ass:append(cglyph)
	else
		local before_cur = ass_escape(chat.line:sub(1, cursor - 1))
		local after_cur = ass_escape(chat.line:sub(cursor))
		ass:append(style .. chat.line_prefix .. before_cur)
		ass:append(cglyph)
		ass:append(style .. after_cur)

		-- Redraw the cursor with the REPL text invisible. This will make the
		-- cursor appear in front of the text.
		ass:new_event()
		ass:an(7)
		ass:pos(2, 2)
		ass:append(style .. '{\\alpha&HFF&}' .. chat.line_prefix .. before_cur)
		ass:append(cglyph)
		ass:append(style .. '{\\alpha&HFF&}' .. after_cur)
	end

	mp.set_osd_ass(screenx, screeny, ass.text)
end

-- Add a line to the log buffer (which is limited to 100 lines)
function chat.msg_add(msg, data)
	if chat.on_message_add then
		chat.on_message_add(msg, data)
	end
end

-- Escape a string for verbatim display on the OSD
function ass_escape(str)
	-- There is no escape for '\' in ASS (I think?) but '\' is used verbatim if
	-- it isn't followed by a recognised character, so add a zero-width
	-- non-breaking space
	str = str:gsub('\\', '\\\239\187\191')
	str = str:gsub('{', '\\{')
	str = str:gsub('}', '\\}')
	-- Precede newlines with a ZWNBSP to prevent ASS's weird collapsing of
	-- consecutive newlines
	str = str:gsub('\n', '\239\187\191\\N')
	-- Turn leading spaces into hard spaces to prevent ASS from stripping them
	str = str:gsub('\\N ', '\\N\\h')
	str = str:gsub('^ ', '\\h')
	return str
end

-- Show the repl if hidden and replace its contents with 'text'
-- (script-message-to repl type)
local function show_and_type(text, cursor_pos)
	text = text or ''
	cursor_pos = tonumber(cursor_pos)

	-- Save the line currently being edited, just in case
	if chat.line ~= text and chat.line ~= '' and chat.history[#chat.history] ~= chat.line then
		chat.history[#chat.history + 1] = chat.line
	end

	chat.line = text
	if cursor_pos ~= nil and cursor_pos >= 1
	    and cursor_pos <= chat.line:len() + 1 then
		cursor = math.floor(cursor_pos)
	else
		cursor = chat.line:len() + 1
	end
	history_pos = #chat.history + 1
	chat.insert_mode = false

	if chat.repl_active then
		chat.update()
	else
		chat.set_active(true)
	end
end

-- Naive helper function to find the next UTF-8 character in 'str' after 'pos'
-- by skipping continuation bytes. Assumes 'str' contains valid UTF-8.
local function next_utf8(str, pos)
	if pos > str:len() then return pos end
	repeat
		pos = pos + 1
	until pos > str:len() or str:byte(pos) < 0x80 or str:byte(pos) > 0xbf
	return pos
end

-- As above, but finds the previous UTF-8 charcter in 'str' before 'pos'
local function prev_utf8(str, pos)
	if pos <= 1 then return pos end
	repeat
		pos = pos - 1
	until pos <= 1 or str:byte(pos) < 0x80 or str:byte(pos) > 0xbf
	return pos
end

-- Insert a character at the current cursor position (any_unicode)
local function handle_char_input(c)
	if chat.insert_mode then
		chat.line = chat.line:sub(1, cursor - 1) .. c .. chat.line:sub(next_utf8(chat.line, cursor))
	else
		chat.line = chat.line:sub(1, cursor - 1) .. c .. chat.line:sub(cursor)
	end
	cursor = cursor + #c
	chat.update()
end

-- Remove the character behind the cursor (Backspace)
local function handle_backspace()
	if cursor <= 1 then return end
	local prev = prev_utf8(chat.line, cursor)
	chat.line = chat.line:sub(1, prev - 1) .. chat.line:sub(cursor)
	cursor = prev
	chat.update()
end

-- Remove the character in front of the cursor (Del)
local function handle_del()
	if cursor > chat.line:len() then return end
	chat.line = chat.line:sub(1, cursor - 1) .. chat.line:sub(next_utf8(chat.line, cursor))
	chat.update()
end

-- Toggle insert mode (Ins)
local function handle_ins()
	chat.insert_mode = not chat.insert_mode
end

-- Move the cursor to the next character (Right)
local function next_char()
	cursor = next_utf8(chat.line, cursor)
	chat.update()
end

-- Move the cursor to the previous character (Left)
local function prev_char()
	cursor = prev_utf8(chat.line, cursor)
	chat.update()
end

-- Clear the current chat.line (Ctrl+C)
function chat.clear()
	chat.line = ''
	cursor = 1
	chat.insert_mode = false
	history_pos = #chat.history + 1
	chat.update()
end

-- Close the REPL if the current chat.line is empty, otherwise delete the next
-- character (Ctrl+D)
local function maybe_exit()
	if chat.line == '' then
		chat.set_active(false)
	else
		handle_del()
	end
end

-- Run the current command and clear the chat.line (Enter)
local function handle_enter()
	if chat.on_input_submit then
		chat.on_input_submit(chat.line)
	end
end

-- Go to the specified position in the command history
local function go_history(new_pos)
	local old_pos = history_pos
	history_pos = new_pos

	-- Restrict the position to a legal value
	if history_pos > #chat.history + 1 then
		history_pos = #chat.history + 1
	elseif history_pos < 1 then
		history_pos = 1
	end

	-- Do nothing if the history position didn't actually change
	if history_pos == old_pos then
		return
	end

	-- If the user was editing a non-history chat.line, save it as the last history
	-- entry. This makes it much less frustrating to accidentally hit Up/Down
	-- while editing a line.
	if old_pos == #chat.history + 1 and chat.line ~= '' and chat.history[#chat.history] ~= chat.line then
		chat.history[#chat.history + 1] = chat.line
	end

	-- Now show the history chat.line (or a blank line for #chat.history + 1)
	if history_pos <= #chat.history then
		chat.line = chat.history[history_pos]
	else
		chat.line = ''
	end
	cursor = chat.line:len() + 1
	chat.insert_mode = false
	chat.update()
end

-- Go to the specified relative position in the command history (Up, Down)
local function move_history(amount)
	go_history(history_pos + amount)
end

-- Go to the first command in the command history (PgUp)
local function handle_pgup()
	go_history(1)
end

-- Stop browsing history and start editing a blank line (PgDown)
local function handle_pgdown()
	go_history(#chat.history + 1)
end

-- Move to the start of the current word, or if already at the start, the start
-- of the previous word. (Ctrl+Left)
local function prev_word()
	-- This is basically the same as next_word() but backwards, so reverse the
	-- string in order to do a "backwards" find. This wouldn't be as annoying
	-- to do if Lua didn't insist on 1-based indexing.
	cursor = chat.line:len() - select(2, chat.line:reverse():find('%s*[^%s]*', chat.line:len() - cursor + 2)) + 1
	chat.update()
end

-- Move to the end of the current word, or if already at the end, the end of
-- the next word. (Ctrl+Right)
local function next_word()
	cursor = select(2, chat.line:find('%s*[^%s]*', cursor)) + 1
	chat.update()
end

-- Move the cursor to the beginning of the line (HOME)
local function go_home()
	cursor = 1
	chat.update()
end

-- Move the cursor to the end of the line (END)
local function go_end()
	cursor = chat.line:len() + 1
	chat.update()
end

-- Delete from the cursor to the beginning of the word (Ctrl+Backspace)
local function del_word()
	local before_cur = chat.line:sub(1, cursor - 1)
	local after_cur = chat.line:sub(cursor)

	before_cur = before_cur:gsub('[^%s]+%s*$', '', 1)
	chat.line = before_cur .. after_cur
	cursor = before_cur:len() + 1
	chat.update()
end

-- Delete from the cursor to the end of the word (Ctrl+Del)
local function del_next_word()
	if cursor > chat.line:len() then return end

	local before_cur = chat.line:sub(1, cursor - 1)
	local after_cur = chat.line:sub(cursor)

	after_cur = after_cur:gsub('^%s*[^%s]+', '', 1)
	chat.line = before_cur .. after_cur
	chat.update()
end

-- Delete from the cursor to the end of the line (Ctrl+K)
local function del_to_eol()
	chat.line = chat.line:sub(1, cursor - 1)
	chat.update()
end

-- Delete from the cursor back to the start of the line (Ctrl+U)
local function del_to_start()
	chat.line = chat.line:sub(cursor)
	cursor = 1
	chat.update()
end

-- Returns a string of UTF-8 text from the clipboard (or the primary selection)
local function get_clipboard(clip)
	if config.platform == 'x11' then
		local res = utils.subprocess({
			args = { 'xclip', '-selection', clip and 'clipboard' or 'primary', '-out' },
			playback_only = false,
		})
		if not res.error then
			return res.stdout
		end
	elseif config.platform == 'wayland' then
		local res = utils.subprocess({
			args = { 'wl-paste', clip and '-n' or '-np' },
			playback_only = false,
		})
		if not res.error then
			return res.stdout
		end
	elseif config.platform == 'windows' then
		local res = utils.subprocess({
			args = { 'powershell', '-NoProfile', '-Command', [[& {
				Trap {
					Write-Error -ErrorRecord $_
					Exit 1
				}

				$clip = ""
				if (Get-Command "Get-Clipboard" -errorAction SilentlyContinue) {
					$clip = Get-Clipboard -Raw -Format Text -TextFormatType UnicodeText
				} else {
					Add-Type -AssemblyName PresentationCore
					$clip = [Windows.Clipboard]::GetText()
				}

				$clip = $clip -Replace "`r",""
				$u8clip = [System.Text.Encoding]::UTF8.GetBytes($clip)
				[Console]::OpenStandardOutput().Write($u8clip, 0, $u8clip.Length)
			}]] },
			playback_only = false,
		})
		if not res.error then
			return res.stdout
		end
	elseif config.platform == 'macos' then
		local res = utils.subprocess({
			args = { 'pbpaste' },
			playback_only = false,
		})
		if not res.error then
			return res.stdout
		end
	end
	return ''
end

-- Paste text from the window-system's clipboard. 'clip' determines whether the
-- clipboard or the primary selection buffer is used (on X11 and Wayland only.)
local function paste(clip)
	local text = get_clipboard(clip)
	local before_cur = chat.line:sub(1, cursor - 1)
	local after_cur = chat.line:sub(cursor)
	chat.line = before_cur .. text .. after_cur
	cursor = cursor + text:len()
	chat.update()
end

local function text_input(info)
	if info.key_text and (info.event == "press" or info.event == "down" or info.event == "repeat") then
		handle_char_input(info.key_text)
	end
end

-- List of input bindings. This is a weird mashup between common GUI text-input
-- bindings and readline bindings.
local function get_bindings()
	local bindings = {
		{ 'esc', function() chat.set_active(false) end },
		{ 'enter', handle_enter },
		{ 'kp_enter', handle_enter },
		{ 'shift+enter', function() handle_char_input('\n') end },
		{ 'ctrl+j', handle_enter },
		{ 'ctrl+m', handle_enter },
		{ 'bs', handle_backspace },
		{ 'shift+bs', handle_backspace },
		{ 'ctrl+h', handle_backspace },
		{ 'del', handle_del },
		{ 'shift+del', handle_del },
		{ 'ins', handle_ins },
		{ 'shift+ins', function() paste(false) end },
		{ 'mbtn_mid', function() paste(false) end },
		{ 'left', function() prev_char() end },
		{ 'ctrl+b', function() prev_char() end },
		{ 'right', function() next_char() end },
		{ 'ctrl+f', function() next_char() end },
		{ 'up', function() move_history(-1) end },
		{ 'ctrl+p', function() move_history(-1) end },
		{ 'wheel_up', function() move_history(-1) end },
		{ 'down', function() move_history(1) end },
		{ 'ctrl+n', function() move_history(1) end },
		{ 'wheel_down', function() move_history(1) end },
		{ 'wheel_left', function() end },
		{ 'wheel_right', function() end },
		{ 'ctrl+left', prev_word },
		{ 'alt+b', prev_word },
		{ 'ctrl+right', next_word },
		{ 'alt+f', next_word },
		{ 'ctrl+a', go_home },
		{ 'home', go_home },
		{ 'ctrl+e', go_end },
		{ 'end', go_end },
		{ 'pgup', handle_pgup },
		{ 'pgdwn', handle_pgdown },
		{ 'ctrl+c', chat.clear },
		{ 'ctrl+d', maybe_exit },
		{ 'ctrl+k', del_to_eol },
		{ 'ctrl+u', del_to_start },
		{ 'ctrl+v', function() paste(true) end },
		{ 'meta+v', function() paste(true) end },
		{ 'ctrl+bs', del_word },
		{ 'ctrl+w', del_word },
		{ 'ctrl+del', del_next_word },
		{ 'alt+d', del_next_word },
		{ 'kp_dec', function() handle_char_input('.') end },
	}

	for i = 0, 9 do
		bindings[#bindings + 1] = { 'kp' .. i, function() handle_char_input('' .. i) end }
	end

	return bindings
end

local function define_key_bindings()
	if #key_bindings > 0 then
		return
	end
	local bindings = get_bindings()
	if chat.on_set_bindings then
		chat.on_set_bindings(bindings)
	end
	for _, bind in ipairs(bindings) do
		-- Generate arbitrary name for removing the bindings later.
		local name = "_console_" .. (#key_bindings + 1)
		key_bindings[#key_bindings + 1] = name
		mp.add_forced_key_binding(bind[1], name, bind[2], { repeatable = true })
	end
	mp.add_forced_key_binding("any_unicode", "_console_text", text_input,
		{ repeatable = true, complex = true })
	key_bindings[#key_bindings + 1] = "_console_text"
end

local function undefine_key_bindings()
	for _, name in ipairs(key_bindings) do
		mp.remove_key_binding(name)
	end
	key_bindings = {}
end

-- Set the REPL visibility ("enable", Esc)
function chat.set_active(active)
	if active == chat.repl_active then
		return
	end
	if active then
		chat.repl_active = true
		chat.insert_mode = false
		if chat.on_open_input then
			chat.on_open_input()
		end
		mp.enable_key_bindings('console-input', 'allow-hide-cursor+allow-vo-dragging')
		mp.enable_messages('terminal-default')
		define_key_bindings()
	else
		chat.repl_active = false
		undefine_key_bindings()
		mp.enable_messages('silent:terminal-default')
		collectgarbage()
	end
	chat.update()
end

-- Add a script-message to show the REPL and fill it with the provided text
mp.register_script_message('type', function(text, cursor_pos)
	show_and_type(text, cursor_pos)
end)

-- Redraw the REPL when the OSD size changes. This is needed because the
-- PlayRes of the OSD will need to be adjusted.
mp.observe_property('osd-width', 'native', chat.update)
mp.observe_property('osd-height', 'native', chat.update)
mp.observe_property('display-hidpi-scale', 'native', chat.update)

return chat
