# Melon Sync
Simple video sync and chat via lua sockets

depends: lua52-socket

## Features:
- [X] syncs seek and pause
- [X] chat (both `list` and `nico nico` style)
- [ ] playlist sharing
- [ ] stream sync

## Todo
- [ ] async message polling? (lua-lanes?)
- [ ] bundle lua52-socket
