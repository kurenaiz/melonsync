local socket = require('socket')
local config = require('config')
local consts = require('consts')
local mputils = require('mp.utils')
local utils = require('utils')
local chat = require('chat')
local client = nil

local server = { users = {}, socket = nil, hosting = false }
local timer
local next_id = 1

-- SERVER FUNCTIONS
-- send 'msg' to all peers except 'ig_id'
function server.send_to_all(msg, ig_id)
	-- print('[server] send_to_all: ' .. msg)
	for id, user in pairs(client.users) do
		for i = 1, #ig_id do
			if ig_id[i] == id then
				goto next
			end
		end
		user.user:send(msg .. '\n')

		::next::
	end
end

function server.send_to_client(msg, id)
	-- print('[server] send_to_client: client_id: ' .. client_id .. ', ' .. msg)
	client.users[id].user:send(msg .. '\n')
end

function server.enter_address_input(input)
	local ip_port = {}
	if input == '' then
		ip_port[1] = config.host_ip
		ip_port[2] = config.host_port
	else
		ip_port = utils.split(input, ':')
		if not ip_port[2] then
			ip_port[2] = config.host_port
		end
	end

	local type = utils.validate_ip(ip_port[1])
	if type == utils.R.ERROR or type == utils.R.STRING then
		mp.osd_message("err" .. type .. ": ip '" .. (ip_port[1] or "nil") .. ":" .. ip_port[2] .. "' is invalid...")
		return
	end

	chat.on_input_submit = nil
	if server.try_host(ip_port[1], ip_port[2]) then
		chat.line_placeholder = ''
		chat.line_prefix = "> "
		chat.set_active(false)
		chat.clear()
		chat.on_join_cb()
	else
		chat.on_input_submit = client.enter_address_input
	end
end

function server.open_address_input(cb)
	if server.hosting then
		print("already hosting...")
		client.show_room()
		return
	end

	chat.on_join_cb = cb
	chat.line_prefix = "host: "
	chat.line_placeholder = config.host_ip .. ":" .. config.host_port
	chat.on_input_submit = server.enter_address_input
	chat.set_active(true)
end

function server.try_host(ip, port)
	ip = ip or config.host_ip
	port = port or config.join_port
	server.socket, err = socket.bind(ip, port)
	if not err then
		client = require('client')
		print("[server] hosting at " .. ip .. ":" .. port)
		mp.osd_message("hosting at " .. ip .. ":" .. port)

		timer = mp.add_periodic_timer(consts.TICK_INTERVAL, server.update)

		mp.add_key_binding('enter', 'repl-enable', function()
			chat.on_input_submit = send_chat_message
			chat.set_active(true)
		end)
		client.users[0] = { id = 0, nick = config.nick, user = nil, }

		client.lock = true
		mp.observe_property("pause", "bool", server.on_pause)
		mp.register_event('playback-restart', server.on_seek)
		server.hosting = true
		return true
	else
		print("error: " .. err)
		mp.osd_message("host failed: " .. err)
	end

	return false

end

function server.exit()
	if server.hosting then
		mp.unregister_event('playback-restart', server.on_seek)
		mp.unobserve_property(server.on_pause)
		timer:kill()
		server.socket:close()
		server.socket = nil
		server.hosting = false
		mp.osd_message("stopped hosting...")
	end
end

function server.update()
	server.socket:settimeout(consts.TICK_INTERVAL)

	local new_user = server.socket:accept()
	if new_user then
		local ip = new_user:getpeername()
		print(ip .. ' joined')
		client.users[next_id] = { id = next_id, user = new_user, nick = "anon", ip = ip }
		next_id = next_id + 1
	end

	for id, user in pairs(client.users) do
		-- if ID is zero, that's us
		if id == 0 then
			goto next
		end

		user.user:settimeout(0)
		local msg, stat = user.user:receive()
		if stat == 'closed' then
			local ip = user.user:getpeername()
			print(user.nick .. ' (' .. ip .. ') left')

			-- sending left message
			local pkt = mputils.format_json({
				['id'] = consts.LEFT_PACKET_ID,
				['userid'] = user.id,
			})
			server.send_to_all(pkt, { client.id, id })

			chat.msg_add(user.nick .. " left", { id = consts.LEFT_PACKET_ID, nick = user.nick })
			client.users[id] = nil
		elseif stat ~= 'timeout' then
			server.handle_message(msg, id);
		end

		::next::
	end
end

-- MESSAGE HANDLING
function server.handle_message(msg, sender_id)
	if msg == '' then
		return
	end

	local res = mputils.parse_json(msg)
	local pktid = res['id']

	if pktid == consts.JOIN_PACKET_ID then
		-- send a joined message to everyone except for the person who joined
		local user = client.users[sender_id]
		user.nick = res['nick']
		local joined_msg = mputils.format_json({
			['id'] = consts.JOINED_PACKET_ID,
			['userid'] = user.id,
			['nick'] = user.nick,
		})
		server.send_to_all(joined_msg, { client.id, sender_id })

		-- local playlist = {}
		-- for i = 0, mp.get_property('playlist-count') - 1, 1 do
		-- 	local name = config.pl_prefix .. '/' .. mp.get_property('playlist/' .. i .. '/filename')
		-- 	-- print('playing ' .. name)
		-- 	table.insert(playlist, name)
		-- end
		local users = {}
		for id, user_i in pairs(client.users) do
			table.insert(users, { nick = user_i.nick, id = user_i.id })
		end

		-- send a sync packet with the current time only to the person who just joined
		local sync_msg = mputils.format_json({
			['id'] = consts.JOIN_SYNC_PACKET_ID,
			['time'] = mp.get_property_native("time-pos"),
			['paused'] = mp.get_property_native("pause"),
			['userid'] = user.id,
			['users'] = users,
			-- ['playlist'] = playlist,
		})
		server.send_to_client(sync_msg, sender_id);

		chat.msg_add(user.nick .. " joined",
			{ id = consts.JOINED_PACKET_ID, nick = user.nick })
		-- server specific packets are handled separately
	else
		client = require('client')
		-- handles the message as a normal client
		client.apply_sync_event(res)
		-- forwards the message
		server.send_to_all(msg, { client.id, sender_id })
	end
end

function server.on_pause(_, v)
	if client.lock then
		client.lock = false
		return
	end

	local msg = mputils.format_json({
		['id'] = consts.PAUSE_PACKET_ID,
		['sender'] = client.id,
		['paused'] = v
	})
	server.send_to_all(msg, { client.id })
end

function server.on_seek(e)
	if client.lock then
		client.lock = false
		return
	end

	-- print('seek time-pos: ' .. mp.get_property_native("time-pos"))

	local time = mp.get_property_native("time-pos")
	local msg = mputils.format_json({
		['id'] = consts.SEEK_PACKET_ID,
		['sender'] = client.id,
		['time'] = time
	})
	server.send_to_all(msg, { client.id })
end

-- TODO
-- function websockify()
-- 	if server then
-- 		-- websockify IP:80 IP:PORT
-- 		io.popen("websockify " .. config.ip .. ":80" .. config.ip .. ":" .. config.port)
-- 	end
-- end

return server
