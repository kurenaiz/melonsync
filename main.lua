-- melonsync.lua
-- MELON SYNC

local config = require('config')

local function setup_chat()
	local style = require('chat_style_' .. config.chat_style)
	if style then
		style.setup()
	else
		print('error: chat style not found. given value: ' .. config.chat_style)
	end
end

local function host()
	local server = require('server')
	local client = require('client')
	if not client.joined then
		server.open_address_input(setup_chat)
	else
		-- mp.osd_message("already joined...")
		client.show_room()
		print("already joined: exit the room before hosting another")
	end
end

local function join()
	local server = require('server')
	local client = require('client')
	if not server.hosting then
		client.open_address_input(setup_chat)
	else
		client.show_room()
		-- mp.osd_message("already hosting...")
		print("already hosting: close the room before joining another")
	end
end

local function exit()
	local server = require('server')
	local client = require('client')
	if server.hosting then
		server.exit()
	elseif client.joined then
		client.exit()
	end
end

function send_chat_message(msg)
	local server = require('server')
	local client = require('client')
	local utils = require('mp.utils')
	local consts = require('consts')
	local chat = require('chat')

	if msg == '' then
		return
	end

	if chat.history[#chat.history] ~= msg then
		chat.history[#chat.history + 1] = msg
	end

	chat.msg_add(config.nick .. ": " .. msg, { id = consts.PAUSE_PACKET_ID, nick = config.nick })
	local pkt = utils.format_json({
		['id'] = consts.MSG_PACKET_ID,
		['sender'] = client.id,
		['msg'] = msg
	})

	if client.joined then
		client.send(pkt)
	elseif server.hosting then
		server.send_to_all(pkt, { client.id })
	end
	chat.clear()
end

-- SYNC EVENTS
-- mp.register_event('file-loaded', on_file_loaded)

-- DEFAULT KEYBINDINGS
-- Add a global binding for enabling the REPL. While it's enabled, its bindings
-- will take over and it can be closed with ESC.
mp.add_key_binding('Ctrl+Shift+h', 'ms-host', host)
mp.add_key_binding('Ctrl+Shift+j', 'ms-join', join)
mp.add_key_binding('Ctrl+Shift+e', 'ms-close', exit)

collectgarbage()
