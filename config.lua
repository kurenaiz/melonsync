local options = require('mp.options')

-- Options can be changed here or in a separate config file.
-- Config path: ~/.config/mpv/script-opts/melonsync.conf
local config = {
	-- All drawing is scaled by this value, including the text borders and the
	-- cursor. Change it if you have a high-DPI display.
	-- Set the font used for the REPL and the console. This probably doesn't
	-- have to be a monospaced font.
	font = 'monospace',
	font_size = 16,
	host_ip = '127.0.0.1',
	host_port = '7171',
	join_ip = '127.0.0.1',
	join_port = '7171',
	pl_prefix = "",
	scale = 1,
	-- Set the font size used for the REPL and the console. This will be
	-- multiplied by "scale."
	nick = "melon",
	chat_timeout = 3,
	chat_style = "list",

	-- TODO: todos
	send_join_info = true,
}

function detect_platform()
	local o = {}
	-- Kind of a dumb way of detecting the platform but whatever
	if mp.get_property_native('options/vo-mmcss-profile', o) ~= o then
		return 'windows'
	elseif mp.get_property_native('options/macos-force-dedicated-gpu', o) ~= o then
		return 'macos'
	elseif os.getenv('WAYLAND_DISPLAY') then
		return 'wayland'
	end
	return 'x11'
end

-- Pick a better default font for Windows and macOS
config.platform = detect_platform()
if config.platform == 'windows' then
	config.font = 'Consolas'
elseif config.platform == 'macos' then
	config.font = 'Menlo'
else
	config.font = 'monospace'
end

options.read_options(config, 'melonsync')

return config
