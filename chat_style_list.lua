local config = require('config')
local chat = require('chat')

local chat_style = {}
local chat_buffer_max = 50
local chat_buffer = {}
local chat_timeout_timer = nil
local chat_active = false

local function render_chat(ass)
	local screenx, screeny, _ = mp.get_osd_size()
	screenx = screenx / config.scale
	screeny = screeny / config.scale

	local style = '{\\r' ..
	    '\\1a&H00&\\3a&H00&\\4a&H99&' ..
	    '\\1c&Heeeeee&\\3c&H111111&\\4c&H000000&' ..
	    '\\fn' .. config.font .. '\\fs' .. config.font_size ..
	    '\\bord2\\xshad0\\yshad1\\fsp0\\q1}'

	-- Render log messages as ASS. This will render at most screeny / font-size
	-- messages.
	local log_ass = ''
	local buffer_size = #chat_buffer
	local log_max_lines = math.ceil(screeny / config.font_size)
	if log_max_lines < buffer_size then
		buffer_size = log_max_lines
	end
	for i = #chat_buffer, 1, -1 do
		log_ass = log_ass .. style .. ass_escape(chat_buffer[i].text)
	end

	ass:new_event()
	ass:an(7)
	ass:pos(2, 2)
	ass:append('\\N' .. log_ass)
end

local function show_text_log_timed(timeout)
	chat_timeout_timer:kill()
	chat_timeout_timer:resume()
	chat.render_bus = render_chat
	chat_active = true
	chat.update()
end

-- this function will run every second, if the chat is active and the input is inactive
-- config.chat_timeout time has passed,  we auto hide the chat
local function disable_chat_buffer_timer()
	if chat_active and not chat.repl_active then
		local screenx, screeny, aspect = mp.get_osd_size()
		screenx = screenx / config.scale
		screeny = screeny / config.scale
		mp.set_osd_ass(screenx, screeny, '')
		chat_active = false
	end
end

local function on_message_add(text, data)
	chat_buffer[#chat_buffer + 1] = { text = text .. '\n' }
	if #chat_buffer > 100 then
		table.remove(chat_buffer, 1)
	end

	show_text_log_timed(config.chat_timeout)
end

-- Empty the log buffer of all messages (Ctrl+L)
local function clear_chat_buffer()
	chat_buffer = {}
	chat.update()
end

local function on_set_bindings(bindings)
	table.insert(bindings, { 'ctrl+l', clear_chat_buffer })
end

local function on_open_input()
	show_text_log_timed()
end

function chat_style.setup()
	-- update_timer = mp.add_periodic_timer(0.05, function()
	-- 	if pending_update then
	-- 		update()
	-- 	else
	-- 		update_timer:kill()
	-- 	end
	-- end)
	-- update_timer:kill()
	chat.on_message_add = on_message_add
	chat.on_set_bindings = on_set_bindings
	chat.on_open_input = on_open_input
	chat_timeout_timer = mp.add_periodic_timer(config.chat_timeout, disable_chat_buffer_timer)
end

function chat_style.destroy()
	chat_buffer = nil
end

return chat_style
