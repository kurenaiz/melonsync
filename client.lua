local socket = require('socket')
local config = require('config')
local consts = require('consts')
local mputils = require('mp.utils')
local chat = require('chat')
local utils = require('utils')

-- `lock` field is a workaround for some mpv behaviour
-- some calls incur on recursive behaviour,
-- e.g. received a pause packet, if we pause the player
-- the observe_property() will trigger, and we'll send a pause packet of our own.
-- e.g. when we first call observe_property(), mpv does an immediate call to
-- the registered event, we don't want to send a pause packet just from start
-- listening to observe_property()
local client = { id = 0, socket = nil, joined = false, lock = false, users = {} }

local timer

function client.enter_address_input(input)
	local ip_port = {}
	if input == '' then
		ip_port[1] = config.join_ip
		ip_port[2] = config.join_port
	else
		ip_port = utils.split(input, ':')
	end

	local type = utils.validate_ip(ip_port[1])
	if type == utils.R.ERROR or type == utils.R.STRING then
		mp.osd_message("invalid ip...")
		return
	end

	chat.on_input_submit = nil
	if client.join(ip_port[1], ip_port[2]) then
		chat.line_prefix = "> "
		chat.line_placeholder = ''
		chat.set_active(false)
		chat.clear()
		chat.on_join_cb()
	else
		chat.on_input_submit = client.enter_address_input
	end
end

function client.open_address_input(cb)
	if client.joined then
		print("already joined...")
		client.show_room()
		return
	end

	chat.on_join_cb = cb
	chat.line_prefix = "join: "
	chat.line_placeholder = config.join_ip .. ":" .. config.join_port
	chat.on_input_submit = client.enter_address_input
	chat.set_active(true)
end

-- prevents the property from triggering itself again
-- e.g. receives a seek message from the server, seeks to the given place, if client.lock=true
-- we do not send another 'seek' message to the server..
local function mp_set_prop_lock(prop, v)
	client.lock = true
	mp.set_property(prop, v)
end

function client.join(ip, port)
	if client.joined then
		mp.osd_message("already joined...")
		return
	end

	client.socket, err = socket.connect(ip, port)
	if not err then
		print("[client] joined...")
		mp.osd_message("joined...")

		timer = mp.add_periodic_timer(consts.TICK_INTERVAL, client.update)

		mp.add_key_binding('enter', 'repl-enable', function()
			chat.on_input_submit = send_chat_message
			chat.set_active(true)
		end)

		-- Sending Join Message
		local msg = mputils.format_json({
			['id'] = consts.JOIN_PACKET_ID,
			['nick'] = config.nick,
		})

		client.send(msg)
		-- mp.register_event('seek', client.on_seek)
		mp.register_event('playback-restart', client.on_seek)

		client.lock = true
		mp.observe_property("pause", "bool", client.on_pause)
		client.joined = true
		return true
	else
		print(string.format([[err: '%s'. ip: '%s' port: '%s' ]], err, ip, port))
		mp.osd_message("failed to join")
		return false
	end
end

function client.show_room()
	local msg = ''
	local count = 0
	for id, user in pairs(client.users) do
		msg = msg ..
		    string.format(" - %s (%s)\n", user.nick, (user.id == client.id and "you" or (user.id == 0 and "host" or user.id)))
		count = count + 1
	end
	msg = count .. " user(s):\n" .. msg
	mp.osd_message(msg)
end

function client.exit()
	if client.joined then
		mp.unregister_event('playback-restart', client.on_seek)
		mp.unobserve_property(client.on_pause)
		timer:kill()
		client.socket:close()
		client.socket = nil
		client.joined = false
		print('[client] exit')
		mp.osd_message("left the room...")
	end
end

function client.send(msg)
	-- print('[client] send: ' .. msg)
	client.socket:send(msg .. '\n')
end

function client.handle_message(msg)
	local ev = mputils.parse_json(msg)
	client.apply_sync_event(ev)
end

function client.apply_sync_event(ev)
	local id = ev['id']
	if id == consts.MSG_PACKET_ID then
		local user = client.users[ev['sender']]
		chat.msg_add(user.nick .. ': ' .. ev['msg'], ev)
	elseif id == consts.SEEK_PACKET_ID then
		local user = client.users[ev['sender']]
		chat.msg_add(user.nick .. " seeked to " .. ev['time'], ev)
		mp_set_prop_lock("time-pos", ev['time'])
	elseif id == consts.PAUSE_PACKET_ID then
		local user = client.users[ev['sender']]
		chat.msg_add(ev['paused'] and user.nick .. " paused" or user.nick .. " unpaused", ev)
		mp_set_prop_lock("pause", ev['paused'] and "yes" or "no")
	elseif id == consts.JOINED_PACKET_ID then
		client.users[ev['userid']] = { nick = ev['nick'], id = ev['userid'] }
		chat.msg_add(ev['nick'] .. " joined", ev)
	elseif id == consts.LEFT_PACKET_ID then
		chat.msg_add(client.users[ev['userid']].nick .. " left", ev)
		client.users[ev['userid']] = nil
	elseif id == consts.SYNC_PACKET_ID then
		mp_set_prop_lock('time-pos', ev['time'])
		mp_set_prop_lock("pause", ev['paused'] and "yes" or "no")
	elseif id == consts.JOIN_SYNC_PACKET_ID then
		mp_set_prop_lock('time-pos', ev['time'])
		mp_set_prop_lock("pause", ev['paused'] and "yes" or "no")
		local myid = ev['userid']
		client.users[myid] = { nick = config.nick, id = myid }
		client.id = myid
		for i = 1, #ev['users'] do
			local user = ev['users'][i]
			client.users[user.id] = { nick = user['nick'], id = user['id'] }
		end
		-- for p in res['playlist']
		-- do
		-- 	print('playlist '..p)
		-- 	-- mp.commandv("loadfile", filename, "append")
		-- end
	end
end

function client.update()
	client.socket:settimeout(consts.TICK_INTERVAL)

	local msg, _ = client.socket:receive()
	if msg then
		client.handle_message(msg)
	end
end

-- sync methods
function client.on_pause(_, v)
	if client.lock then
		client.lock = false
		return
	end

	local msg = mputils.format_json({
		['id'] = consts.PAUSE_PACKET_ID,
		['sender'] = client.id,
		['paused'] = v
	})
	client.send(msg)
end

function client.on_seek(_)
	if client.lock then
		client.lock = false
		return
	end

	local time = mp.get_property_native("time-pos")
	local msg = mputils.format_json({
		['id'] = consts.SEEK_PACKET_ID,
		['sender'] = client.id,
		['time'] = time
	})
	client.send(msg)
end

return client
